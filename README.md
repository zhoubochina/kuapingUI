# 跨屏UI框架

#### 介绍
跨屏UI框架是基于Bootstrap的扩展UI框架。如果说Boostrap提供了一个可靠的网页元素UI组件，来构建一个网页，那么跨屏UI框架，则在bootstrap基础上提供了更为完整的网页组件，例如“关于我们”、“联系我们”，“相册”，“产品”等等组件。

 **http://ui.kuaping.com**

### 截图
![输入图片说明](https://images.gitee.com/uploads/images/2020/1030/165241_e1a33f5f_111545.png "微信截图_20201029084531.png")

#### 软件架构
基于Bootstrap v3.0版本搭建


#### 安装教程

1.  引入bootstrap样式文件，和kuapingUI样式文件 

    `<link rel="stylesheet" href="css/bootstrap-3.3.7.min.css" >`

    `<link rel="stylesheet" href="css/websiteCSS.css" class="reloadable-css" type="text/css">`

2. 
  引入bootstrap脚本文件 依赖jquery和jquery.easing，和kuapingUI脚本文件

  `<script src="js/jquery.min-1.11.2.js" type="text/javascript" charset="utf-8"></script>`

`<script src="js/jquery.easing-1.4.1.min.js" type="text/javascript" charset="utf-8"></script>`

`<script src="js/bootstrap.min-3.37.js" type="text/javascript" charset="utf-8"></script>`

`<script src="js/websiteJS.js" type="text/javascript" charset="utf-8"></script>`



#### 使用说明

1.  参考bootstrap v3.0文档规范
2.  参考kuapingUI 组件演示


#### 参与贡献

1.  kuapingwang
2.  qietuwang

 

